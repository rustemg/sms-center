﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    public static class Paths
    {
        public const string ControlFile = "..\\..\\db\\control";
        public const string Timetable   = "..\\..\\db\\uit-1-10.xml";
        public const string SmsInLog    = "..\\..\\db\\sms_in";
        public const string SmsOutLog   = "..\\..\\db\\sms_out";
        public const string ATLog       = "..\\..\\db\\at";
        public const string UserList    = "..\\..\\db\\users";
    }
}