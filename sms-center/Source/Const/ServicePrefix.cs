﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    public static class ServicePrefix
    {
        public const string Timetable = "расп";
        public const string Informer = ".";
        public const string Register = "рег";
    }
}
