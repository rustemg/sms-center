﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    public static class UserType
    {
        public const int Student = 0;
        public const int Teacher = 1;
    }
}
