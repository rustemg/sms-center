﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InfoCenterSMS.Services;

namespace InfoCenterSMS
{
    class ServiceManager
    {
        private List<User> userList;
        private TimetableManager timetableManager;
        private Logger logger;

        public ServiceManager()
        {
            userList = new List<User>();
            timetableManager = new TimetableManager(userList);
            getUserList();
            logger = new Logger(Paths.SmsInLog);
        }

        //==================================================================================

        public List<SMS> ProcessSMS(List<SMS> smsList)
        {
            string answer = "";
            var answerList = new List<SMS>();
            foreach (var sms in smsList)
            {
                logger.Write(sms);
                if (IsPhoneNumberValid(sms.Sender))
                {
                    List<string> smsText = sms.Text.Split(new string[] { " " }
                                        , StringSplitOptions.RemoveEmptyEntries).ToList();

                    string req = smsText[0].ToLower();
                    if (req == ServicePrefix.Timetable)
                    {
                        answer = timetableManager.ParseSMS(sms);
                        if (IsAnswerValid(answer))
                        {
                            answerList.Add(new SMS(sms.Sender, answer));
                        }
                    }
                    else if (req == ServicePrefix.Informer)
                    {
                        foreach (var user in userList)
                        {
                            if (user.Type == UserType.Student)
                            {
                                answerList.Add(new SMS(user.PhoneNumber, sms.Text
                                                    .Remove(0, 2)));
                            }
                        }
                    }
                    else if (req == ServicePrefix.Register)
                    {
                        if (smsText.Count == 4)
                        {
                            userList.Add(new User(UserType.Student
                                    , sms.Sender
                                    , smsText[1]
                                    , smsText[2]
                                    , smsText[3]));
                            System.IO.File.AppendAllText(Paths.UserList
                                ,"\r\n0, " 
                                + sms.Sender + ", "
                                + smsText[1] + ", "
                                + smsText[2] + ", "
                                + smsText[3]);
                            answerList.Add(new SMS(sms.Sender, "reg ok"));
                        } 
                    }
                }
            }

            return answerList;
        }

        //==================================================================================

        private bool IsPhoneNumberValid(string phNum)
        {
            return (phNum.Length == 12) && (phNum.Substring(0, 3) == "+79");
        }

        //==================================================================================

        private bool IsAnswerValid(string answer)
        {
            return (!(string.IsNullOrEmpty(answer) || string.IsNullOrWhiteSpace(answer)));
        }

        //==================================================================================

        private void getUserList()
        {
            string[] users = System.IO.File.ReadAllLines(Paths.UserList);
            foreach (var user in users)
            {
                string[] info = user.Split(new string[] { "," }
                                        , StringSplitOptions.None);
                userList.Add(new User(Convert.ToInt32(info[0])
                                    , info[1]
                                    , info[2]
                                    , info[3]
                                    , info[4]));
            }
        }
    }
}