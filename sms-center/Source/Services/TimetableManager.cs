﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS.Services
{
    class TimetableManager
    {
        private Timetable timeTable;
        private List<User> userList;


        public TimetableManager(List<User> users)
        {
            userList = users;
            timeTable = new Timetable(Paths.Timetable);
        }

        public string ParseSMS(SMS sms)
        {
            string subgroup = "";
            string answer = "";
            List<string> parts = sms.Text.Split(new string[] { " " }
                                        , StringSplitOptions.RemoveEmptyEntries).ToList();
            if (parts[0].ToLower() != ServicePrefix.Timetable)
            {
                return string.Empty;
            }

            var usr = userList.Find(u => u.PhoneNumber == sms.Sender);

            if (usr != null)
            {
                subgroup = usr.Subgroup;
            }
            answer = timeTable.GetTodayLessons(subgroup);
            return answer;
        }
    }
}
