﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace InfoCenterSMS.Services
{
    class Timetable
    {
        private XmlDocument xmlDoc;
        private int startWeek;
        private DateTime startDate;


        public Timetable(string sourceFile)
        {
            xmlDoc = new XmlDocument();
            xmlDoc.Load(sourceFile);

            XmlNode rootNode = xmlDoc.SelectSingleNode("timetable");
            startWeek = Convert.ToInt32(rootNode.Attributes["startweek"].Value);
            startDate = DateTime.Parse(rootNode.Attributes["startdate"].Value);
        }

        //==================================================================================

        public string GetTodayLessons(string subgroup = "")
        {
            int dayId = (int)DateTime.Today.DayOfWeek;
            return getLessons(dayId, weekType(), subgroup);
        }

        //==================================================================================

        public string GetTomorrowLessons(string subgroup = "")
        {
            int dayId = (int)DateTime.Today.DayOfWeek + 1;
            if (dayId > 7) 
            {
                dayId = 1;
            }
            return getLessons(dayId, weekType(), subgroup);
        }

        //==================================================================================

        private string getLessons(int dayId, string weekType, string subgroup)
        {
            var requestDay = string.Format("timetable/day[@id = {0}]", dayId);

            var requestSelectByWeek
                = string.Format("(@weeks=\"all\" or @weeks=\"{0}\")", weekType);

            var requestSelectBySubgroup
                = string.Format("(@subgroup=0 or @subgroup={0})", subgroup);

            string requestSelectBySpecialWeeks
                = "(@weeks != \"all\" and @weeks != \"odd\" and @weeks != \"even\")";

            var requestCommonLessons
                = string.Format("lesson[{0} and {1}]"
                                , requestSelectByWeek
                                , requestSelectBySubgroup);

            string requestSpecialLessons
                = string.Format("lesson[{0} and {1}]"
                                , requestSelectBySpecialWeeks
                                , requestSelectBySubgroup);
            
            XmlNode dayNode = xmlDoc.SelectSingleNode(requestDay);
            XmlNodeList commonLessonNodes
                = dayNode.SelectNodes(requestCommonLessons);
            XmlNodeList specialLessonsNodes
                = dayNode.SelectNodes(requestSpecialLessons);

            var lessons = new List<string>();
            foreach (XmlNode lessonNode in commonLessonNodes)
            {
                lessons.Add(parseLessonNode(lessonNode));
            }
            foreach (XmlNode lessonNode in specialLessonsNodes)
            {
                if (checkSpecialLessons(lessonNode))
                {
                    lessons.Add(parseLessonNode(lessonNode));
                }
            }
            
            lessons.Sort();
            return string.Join("\r\n", lessons);
        }

        //==================================================================================

        public int CurrentWeek()
        {
            string deltaString = Convert.ToString(DateTime.Today - startDate);
            int dayDelta 
                    = Convert.ToInt32(deltaString.Substring(0, deltaString.IndexOf(".")));
            return startWeek + dayDelta / 7;
        }

        //==================================================================================

        private bool checkSpecialLessons(XmlNode lessonNode)
        {
            string currentWeek = Convert.ToString(CurrentWeek());
            string[] weeks = lessonNode.Attributes["weeks"].Value
                            .Split(new string[]{","}
                            , StringSplitOptions.RemoveEmptyEntries);
            return weeks.Contains(currentWeek);
        }

        //==================================================================================

        private string parseLessonNode(XmlNode lessonNode)
        {
            string lessonName = lessonNode.SelectSingleNode("name").InnerText;
            string lessonPlace = lessonNode.SelectSingleNode("aud").InnerText;
            string lessonTime
                = lessonNode.SelectSingleNode("time").InnerText.Substring(0, 5);
            return lessonTime + ", " + lessonName + ", " + lessonPlace;
        }

        //==================================================================================

        private string weekType()
        {
            string weekType = "";
            switch (CurrentWeek() % 2)
            {
                case 0: weekType = "even"; break;
                case 1: weekType = "odd"; break;
            }
            return weekType;
        }
    }
}
