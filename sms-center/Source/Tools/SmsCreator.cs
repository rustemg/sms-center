﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    class SmsCreator
    {
        private List<SMS> smsList;
        private Dictionary<string, LongSMS> incompleteSms;


        public SmsCreator()
        {
            smsList = new List<SMS>();
            incompleteSms = new Dictionary<string, LongSMS>();
        }

        //==================================================================================

        public List<SMS> GenerateSMS(string data)
        {
            smsList.Clear();

            string[] rawSmsList = data.Split(new string[] { "\r\n" }
                                    , StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < rawSmsList.Length; i++)
            {
                if (rawSmsList[i].Contains("+CMGL:"))
                {
                    if (isPartialSms(rawSmsList[i + 1]))
                    {
                        processIncompleteSms(rawSmsList[i], rawSmsList[i + 1]);
                    }
                    else
                    {
                        addToSmsList(rawSmsList[i], rawSmsList[i + 1]);
                    }
                    i++;
                }
            }

            return smsList;
        }

        //==================================================================================

        private void addToSmsList(string header, string text)
        {
            smsList.Add(new SMS(getPhoneNumber(header)
                                , UCS2.Decode(text)
                                , getDate(header)));
        }

        //==================================================================================

        private void processIncompleteSms(string header, string text)
        {
            LongSMS longSms;

            string id = text.Substring(0, 8);
            if (incompleteSms.ContainsKey(id))
            {
                longSms = incompleteSms[id];
                longSms.AddNewSms(text);
                if (longSms.IsComplete())
                {
                    addToSmsList(longSms.Header, longSms.GetText());
                    incompleteSms.Remove(id);
                }
            }
            else
            {
                incompleteSms.Add(id, new LongSMS(header, text));
            }
        }

        //==================================================================================

        private bool isPartialSms(string smsText)
        {
            bool ok = false;
            if (smsText.Length > 3)
            {
                ok = smsText.Substring(0, 4) == "0500";
            }
            return ok;
        }

        //==================================================================================

        private string getPhoneNumber(string header)
        {
            
            return UCS2.Decode(parseHeader(header, 2));
        }

        //==================================================================================

        private string getDate(string header)
        {
            return parseHeader(header, 4) 
                    + " " + parseHeader(header, 5) ;
        }

        //==================================================================================

        private string parseHeader(string header, int index)
        {
            string[] parts = header.Split(new string[] { "," }
                                        , StringSplitOptions.None);
            return parts[index].Replace("\"", string.Empty);
        }
    }
}
