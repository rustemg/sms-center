﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    class Logger
    {
        private string fileName;
        private string delimeter = "\r\n\r\n//====================================//\r\n\r\n";

        public Logger(string logFile)
        {
            fileName = logFile;
        }

        public void Write(string info)
        {
            System.IO.File.AppendAllText(fileName, info + delimeter);
        }

        public void Write (SMS sms)
        {
            Write(sms.Time + "\r\n" + sms.Sender + "\r\n" + sms.Text);
        }
    }
}
