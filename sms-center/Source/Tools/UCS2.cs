﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    static class UCS2
    {
        public static string Encode(string text)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char ch in text)
            {
                int dec = (int)ch;
                string hex = dec.ToString("X");
                while (hex.Length < 4)
                {
                    hex = "0" + hex;
                }
                builder.Append(hex);
            }
            return builder.ToString();
        }

        public static string Decode(string text)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < text.Length; i = i + 4)
            {
                string hexCode = text.Substring(i, 4);
                int decCode = Convert.ToInt32(hexCode, 16);
                char ch = (char)decCode;
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
}
