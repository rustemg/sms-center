﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    class User
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Subgroup { get; private set; }
        public int    Type { get; private set; }

        public User(int userType
                    , string phNum
                    , string fName
                    , string lName
                    , string sGroup)
        {
            PhoneNumber = phNum.Trim();
            FirstName = fName.Trim();
            LastName = lName.Trim();
            Type = userType;
            Subgroup = sGroup.Trim();
        }
    }
}
