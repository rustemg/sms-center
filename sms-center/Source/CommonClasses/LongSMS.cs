﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    class LongSMS
    {
        public int Segments { get; private set; }
        public string Header { get; private set; }

        private string[] parts;


        public LongSMS(string header, string text)
        {
            Segments = Convert.ToInt32(text.Substring(8, 2));
            parts = new string[Segments];
            Header = header;
            AddNewSms(text);
        }


        public bool IsComplete()
        {
            foreach (string str in parts)
            {
                if (str == "")
                {
                    return false;
                }
            }
            return true;
        }


        public void AddNewSms(string text)
        {
            int index = Convert.ToInt32(text.Substring(10, 2)) - 1;
            parts[index] = text.Remove(0, 12);
        }

        public string GetText()
        {
            string text = string.Empty;            
            foreach (var str in parts)
            {
                text += str;
            }
            return text;
        }
    }
}
