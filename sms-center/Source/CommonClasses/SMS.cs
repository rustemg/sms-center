﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfoCenterSMS
{
    class SMS
    {
        public string Sender { get; private set; }
        public string Text { get; private set; }
        public string Time { get; private set; }

        public SMS(string phoneNumber, string text, string time = "")
        {
            Sender = phoneNumber;
            Text = text;
            Time = time;
        }
    }
}
