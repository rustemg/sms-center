﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;

namespace InfoCenterSMS
{
    class Modem
    {
        private SerialPort serialPort;
        private Logger smsLogger;
        private Logger atLogger;

        //==================================================================================

        public Modem(string portName)
        {
            serialPort = new SerialPort(portName);
            smsLogger = new Logger(Paths.SmsOutLog);
            atLogger = new Logger(Paths.ATLog);
        }

        //==================================================================================

        public bool InitModem()
        {
            try
            {
                serialPort.Open();
            }
            catch (IOException)
            {
                Console.WriteLine(serialPort.PortName + ": Невозможно подключиться к модему");
            }
            
            if (!serialPort.IsOpen)
            {
                return false;
            }

            string answer;
            string[] initialCommands = new string[] 
            { 
                "AT",
                "AT+CMGF=1",
                "AT+CSMS=0",
                "AT+CSCS=\"UCS2\"",
                "AT+CSMP=17,167,0,25"
            };

            foreach (var comm in initialCommands)
            {
                answer = SendCommandToModem(comm);
                if (getCommandStatus(answer) != "OK")
                {
                    return false;
                }
            }
            return true;
        }

        //==================================================================================

        public void SendSMS(string phoneNumber, string smsText)
        {
            List<string> partList = new List<string>();
            int partsCount = 1;
            smsText = UCS2.Encode(smsText);
            partList.Add(smsText);
            if (smsText.Length / 4 > 70) 
            {
                partList.Clear();
                partsCount = smsText.Length / 4 / 67 + 1;
                int len = smsText.Length;
                for (int i = 0; i < partsCount; i++)
                {
                    int l = 67 * 4;
                    if (len < l)
                    {
                        l = len;
                    }
                    partList.Add(smsText.Substring(i * 67 * 4, l));
                    len -= 67 * 4;
                }
            }

            for (int i = 0; i < partsCount; i++)
            {
                SendCommandToModem("AT+CMGS=\"" + UCS2.Encode(phoneNumber) + "\"");
                SendCommandToModem(partList[i] + "\x1A");
                smsLogger.Write(DateTime.Now.ToString() 
                            + "\r\n" + phoneNumber 
                            + "\r\n" + UCS2.Decode(partList[i]));
            }
        }

        //==================================================================================

        public void SendSMS(SMS sms)
        {
            SendSMS(sms.Sender, sms.Text);
        }

        //==================================================================================

        public void SendSMS(List<SMS> smsList)
        {
            foreach (var sms in smsList)
            {
                SendSMS(sms.Sender, sms.Text);
            }
        }

        //==================================================================================

        public string ReadRawSMS()
        {
            return SendCommandToModem("AT+CMGL=\"ALL\"");
        }

        //==================================================================================

        public void DeleteSMS()
        {
            SendCommandToModem("AT+CMGD=,1");
        }

        //==================================================================================

        public string SendCommandToModem(string comm)
        {
            System.Threading.Thread.Sleep(2000);
            serialPort.Write(comm + "\r");

            System.Threading.Thread.Sleep(2000);
            string answer = serialPort.ReadExisting();

            atLogger.Write(DateTime.Now.ToString() + "\r\n" + comm + answer);
            answer = answer.Trim();
            answer = answer.Replace(comm + "\r\n\r\n", string.Empty);
            return answer;
        }

        //==================================================================================

        public void Disconnect()
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
            }
        }

        //==================================================================================

        private string getCommandStatus(string answer)
        {
            string[] answers = answer.Split(new string[] { "\r\n" } 
                                , StringSplitOptions.RemoveEmptyEntries);

            return answers[answers.Length - 1];
        }

        //==================================================================================

        public void EnableTerminal()
        {
            while (true)
            {
                Console.Write(">>> ");
                string com = Console.ReadLine().ToUpper();
                if (com == "EXIT")
                {
                    break;
                }
                SendCommandToModem(com);
            }
        }
    }
}