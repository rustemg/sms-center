﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InfoCenterSMS.Services;

namespace InfoCenterSMS
{
    class Program
    {
        static void Main(string[] args)
        {
            var smsCreator = new SmsCreator();
            var serviceManager = new ServiceManager();
            var modem = new Modem("COM14");
            
            if (modem.InitModem())
            {
                while (true)
                {
                    string rawSms = modem.ReadRawSMS();
                    List<SMS> smsList = smsCreator.GenerateSMS(rawSms);
                    if (smsList.Count > 0)
                    {
                        modem.DeleteSMS();
                        modem.SendSMS(serviceManager.ProcessSMS(smsList));
                    }

                    System.Threading.Thread.Sleep(10000);
                    string[] on = System.IO.File.ReadAllLines(Paths.ControlFile);                    
                    if (on[0] == "off") { break; }
                }
                //modem.EnableTerminal();
            }
            modem.Disconnect();
        }
    }
}
